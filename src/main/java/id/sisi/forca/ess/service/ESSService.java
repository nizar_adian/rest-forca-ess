package id.sisi.forca.ess.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.eclipse.jetty.util.StringUtil;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.query.internal.NativeQueryImpl;
import org.hibernate.transform.AliasToEntityMapResultTransformer;

import id.sisi.forca.ess.model.entity.AccessToken;
import id.sisi.forca.ess.model.form.request.LoginForm;
import id.sisi.forca.ess.model.form.request.UserInfoForm;
import id.sisi.forca.ess.model.form.response.ResponseData;

public class ESSService {
	private SessionFactory sessionFactory;

	public ESSService(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public ResponseData login(LoginForm login) {
		if (StringUtil.isEmpty(login.getUsername()) || 
				StringUtil.isEmpty(login.getPassword())) {
			return ResponseData.errorResponse("Authentication Failed");
		}
		
		String sql = 
				"select \n" + 
				"res_users.id,\n" + 
				"res_users.login,\n" + 
				"res_users.forca_person_id,\n" + 
				"res_users.device_id\n" + 
				"from \n" + 
				"res_users \n" + 
				"where login = :username and password_crypt = :password";
		
		Query q = sessionFactory.getCurrentSession().createNativeQuery(sql);
		q.setParameter("username", login.getUsername());
		q.setParameter("password", login.getPassword());
		Optional<Object> result = Optional.ofNullable(q.uniqueResult());
		if (!result.isPresent()) {
			return ResponseData.errorResponse("Authentication Failed");
		}
		Object[] data = (Object[]) result.get();
		deleteTokenExisting((int) data[0]);
		HashMap<String, String> map = new HashMap<String, String>();
		AccessToken accessToken = createToken((int) data[0]);
		map.put("ad_user_id", String.valueOf(data[0]));
		map.put("ad_user_name", String.valueOf(data[1]));
		map.put("ad_client_id", null);
		map.put("ad_client_name", null);
		map.put("forca_person_id", String.valueOf(data[2]));
		map.put("forca_deviceid", String.valueOf(data[3]));
		map.put("ad_org_id", "1");
		map.put("ad_role_id", null);
		map.put("salesrep_ID", null);
		map.put("m_warehouse_ID", null);
		map.put("token", accessToken.getToken());
		return ResponseData.successResponse("", map);
	}

	private AccessToken createToken(int id) {
		AccessToken accessToken = new AccessToken();
		accessToken.setUser_id(id);
		accessToken.setToken(UUID.randomUUID().toString());
		accessToken.setScope("userinfo");
		accessToken.setCreate_date(new Date(System.currentTimeMillis()));
		accessToken.setCreate_uid(1);
		accessToken.setWrite_date(new Date(System.currentTimeMillis()));
		accessToken.setWrite_uid(1);
		sessionFactory.getCurrentSession().persist(accessToken);
		return accessToken;
	}

	private void deleteTokenExisting(int userID) {
		String sqlTokenExist = "select id from api_access_token where user_id = :user_id";
		Query q = sessionFactory.getCurrentSession().createNativeQuery(sqlTokenExist);
		q.setParameter("user_id", userID);
		List<Object> result = q.list();
		for (Object o : result) {
			int id = (int) o;
			AccessToken accessToken = new AccessToken();
			accessToken.setId(id);
			sessionFactory.getCurrentSession().delete(accessToken);
		}
	}

	public void logout(String token) {
		Optional<Object> result = getToken(token);
		if (result.isPresent()) {
			int id = (int) result.get();
			AccessToken accessToken = new AccessToken();
			accessToken.setId(id);
			sessionFactory.getCurrentSession().delete(accessToken);
		}
	}

	public Optional getToken(String token) {
		String sql = "select id from api_access_token where token = :token";
		Query q  = sessionFactory.getCurrentSession().createNativeQuery(sql);
		q.setParameter("token", token);
		Optional result = q.uniqueResultOptional();
		return result;
	}

	public ResponseData getUserInfo(UserInfoForm form) {
		String sql = 
				"select \n" + 
				"CAST('' AS varchar(50)) as image,\n" + 
				"coalesce(hr_employee.barcode,'') as nik,\n" + 
				"coalesce(hr_employee.name,'') as name,\n" + 
				"coalesce(hr_employee.work_email,'') as work_email,\n" + 
				"coalesce(hr_employee.work_phone,'') as phone,\n" + 
				"case\n" + 
				"when res_users.isusefacedetection then 'Y'\n" + 
				"else 'N' \n" + 
				"end as isusefacedetection,\n" + 
				"coalesce(res_users.latitude,'') as latitude,\n" + 
				"coalesce(res_users.longitude,'') as longitude,\n" + 
				"case\n" + 
				"when res_users.ischecklocation then 'Y'\n" + 
				"else 'N' \n" + 
				"end as ischecklocation\n" + 
				"from res_users\n" + 
				"left join hr_employee on hr_employee.id = res_users.partner_id\n" + 
				"where res_users.id = :user_id";
		Query q = sessionFactory.getCurrentSession().createNativeQuery(sql);
		q.setParameter("user_id", Integer.valueOf(form.getAd_user_id()));
		NativeQueryImpl nativeQuery = (NativeQueryImpl) q;
		nativeQuery.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
		Optional result = Optional.ofNullable(nativeQuery.getSingleResult());
		if (result.isPresent()) {
			return ResponseData.successResponse("1 data found", result.get());
		} else {
			return ResponseData.errorResponse("no data found");
		}
	}

}
