package id.sisi.forca.ess.model.form.request;

import java.io.Serializable;

public class DeviceForm implements Serializable{

	private static final long serialVersionUID = 8944662405805981690L;
	private String ad_user_id;
	private String forca_deviceid;

	public String getAd_user_id() {
		return ad_user_id;
	}

	public void setAd_user_id(String ad_user_id) {
		this.ad_user_id = ad_user_id;
	}

	public String getForca_deviceid() {
		return forca_deviceid;
	}

	public void setForca_deviceid(String forca_deviceid) {
		this.forca_deviceid = forca_deviceid;
	}

}
