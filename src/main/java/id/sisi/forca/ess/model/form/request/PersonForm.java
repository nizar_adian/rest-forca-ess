package id.sisi.forca.ess.model.form.request;

import java.io.Serializable;

public class PersonForm implements Serializable{

	private static final long serialVersionUID = -2120919843433486645L;
	private String ad_user_id;
	private String forca_person_id;

	public String getAd_user_id() {
		return ad_user_id;
	}

	public void setAd_user_id(String ad_user_id) {
		this.ad_user_id = ad_user_id;
	}

	public String getForca_person_id() {
		return forca_person_id;
	}

	public void setForca_person_id(String forca_person_id) {
		this.forca_person_id = forca_person_id;
	}

}
