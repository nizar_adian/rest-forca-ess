package id.sisi.forca.ess.model.form.request;

import java.io.Serializable;

public class AttendanceForm implements Serializable{

	private String nik;
	private String name;
	private String status;
	private String imageurl;
	private String latitude;
	private String longitude;
	private String forca_distance;
	private String forca_branch_id;
	private String forca_device_id;
	private String forca_devicemodel;
	private String description;
	private String forca_location;

	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getImageurl() {
		return imageurl;
	}

	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getForca_distance() {
		return forca_distance;
	}

	public void setForca_distance(String forca_distance) {
		this.forca_distance = forca_distance;
	}

	public String getForca_branch_id() {
		return forca_branch_id;
	}

	public void setForca_branch_id(String forca_branch_id) {
		this.forca_branch_id = forca_branch_id;
	}

	public String getForca_device_id() {
		return forca_device_id;
	}

	public void setForca_device_id(String forca_device_id) {
		this.forca_device_id = forca_device_id;
	}

	public String getForca_devicemodel() {
		return forca_devicemodel;
	}

	public void setForca_devicemodel(String forca_devicemodel) {
		this.forca_devicemodel = forca_devicemodel;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getForca_location() {
		return forca_location;
	}

	public void setForca_location(String forca_location) {
		this.forca_location = forca_location;
	}

}
