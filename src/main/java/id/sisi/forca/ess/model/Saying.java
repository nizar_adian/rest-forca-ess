package id.sisi.forca.ess.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Saying {
	
	@JsonProperty
	private String message;
	
	public Saying(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
