package id.sisi.forca.ess.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "api_access_token")
public class AccessToken implements Serializable {

	private static final long serialVersionUID = 8410432227931017329L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "token")
	private String token;

	@Column(name = "token_logout")
	private boolean token_logout;

	@Column(name = "user_id")
	private int user_id;

	@Column(name = "expires")
	private Date expires;

	@Column(name = "scope")
	private String scope;

	@Column(name = "create_uid")
	private int create_uid;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date")
	private Date create_date;

	@Column(name = "write_uid")
	private int write_uid;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "write_date")
	private Date write_date;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public boolean isToken_logout() {
		return token_logout;
	}

	public void setToken_logout(boolean token_logout) {
		this.token_logout = token_logout;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public Date getExpires() {
		return expires;
	}

	public void setExpires(Date expires) {
		this.expires = expires;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public int getCreate_uid() {
		return create_uid;
	}

	public void setCreate_uid(int create_uid) {
		this.create_uid = create_uid;
	}

	public Date getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}

	public int getWrite_uid() {
		return write_uid;
	}

	public void setWrite_uid(int write_uid) {
		this.write_uid = write_uid;
	}

	public Date getWrite_date() {
		return write_date;
	}

	public void setWrite_date(Date write_date) {
		this.write_date = write_date;
	}

}
