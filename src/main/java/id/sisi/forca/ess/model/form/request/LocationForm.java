package id.sisi.forca.ess.model.form.request;

import java.io.Serializable;

public class LocationForm implements Serializable{
	
	private static final long serialVersionUID = 4114752885852769460L;
	private String ad_user_id;

	public String getAd_user_id() {
		return ad_user_id;
	}

	public void setAd_user_id(String ad_user_id) {
		this.ad_user_id = ad_user_id;
	}
	
	

}
