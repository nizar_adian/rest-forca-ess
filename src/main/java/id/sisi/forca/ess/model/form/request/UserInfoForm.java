package id.sisi.forca.ess.model.form.request;

import java.io.Serializable;

public class UserInfoForm implements Serializable{
	
	private static final long serialVersionUID = -3701464286791591025L;
	private String ad_user_id;

	public String getAd_user_id() {
		return ad_user_id;
	}

	public void setAd_user_id(String ad_user_id) {
		this.ad_user_id = ad_user_id;
	}
	
	

}
