package id.sisi.forca.ess.model.entity;

public enum StateCheck {
	ontime,
	late,
	latefast,
	warning
}
