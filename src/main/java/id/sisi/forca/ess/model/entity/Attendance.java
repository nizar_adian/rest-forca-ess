package id.sisi.forca.ess.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "forca_hr_attendance_tmp")
public class Attendance implements Serializable {

	private static final long serialVersionUID = -2087019632235625274L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "employee_id")
	private int employee_id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_check")
	private Date date_check;

	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private StatusCheck status;

	@Enumerated(EnumType.STRING)
	@Column(name = "state")
	private StateCheck state;

	@Column(name = "longitude")
	private String longitude;

	@Column(name = "latitude")
	private String latitude;

	@Column(name = "branch")
	private int branch;

	@Column(name = "distance")
	private String distance;

	@Column(name = "device_id")
	private String device_id;

	@Column(name = "device_model")
	private String device_model;

	@Column(name = "location")
	private String location;

	@Column(name = "description")
	private String description;

	@Column(name = "imageurl")
	private String imageurl;

	@Column(name = "create_uid")
	private int create_uid;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date")
	private Date create_date;

	@Column(name = "write_uid")
	private int write_uid;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "write_date")
	private Date write_date;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(int employee_id) {
		this.employee_id = employee_id;
	}

	public Date getDate_check() {
		return date_check;
	}

	public void setDate_check(Date date_check) {
		this.date_check = date_check;
	}

	public StatusCheck getStatus() {
		return status;
	}

	public void setStatus(StatusCheck status) {
		this.status = status;
	}

	public StateCheck getState() {
		return state;
	}

	public void setState(StateCheck state) {
		this.state = state;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public int getBranch() {
		return branch;
	}

	public void setBranch(int branch) {
		this.branch = branch;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public String getDevice_id() {
		return device_id;
	}

	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}

	public String getDevice_model() {
		return device_model;
	}

	public void setDevice_model(String device_model) {
		this.device_model = device_model;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageurl() {
		return imageurl;
	}

	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}

	public int getCreate_uid() {
		return create_uid;
	}

	public void setCreate_uid(int create_uid) {
		this.create_uid = create_uid;
	}

	public Date getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}

	public int getWrite_uid() {
		return write_uid;
	}

	public void setWrite_uid(int write_uid) {
		this.write_uid = write_uid;
	}

	public Date getWrite_date() {
		return write_date;
	}

	public void setWrite_date(Date write_date) {
		this.write_date = write_date;
	}

}
