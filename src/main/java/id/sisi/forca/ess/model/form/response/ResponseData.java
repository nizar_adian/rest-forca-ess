package id.sisi.forca.ess.model.form.response;

import java.io.Serializable;
import java.util.ArrayList;

public class ResponseData implements Serializable{

	private static final long serialVersionUID = 1247563301950983319L;
	private String codestatus;
	private String message;
	private Object resultdata = new ArrayList<Object>();
	
	public String getCodestatus() {
		return codestatus;
	}

	public void setCodestatus(String codestatus) {
		this.codestatus = codestatus;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getResultdata() {
		return resultdata;
	}

	public void setResultdata(Object resultdata) {
		this.resultdata = resultdata;
	}
	
	public static ResponseData successResponse(String msg, Object result) {
		ResponseData response = new ResponseData();
		response.setCodestatus("S");
		response.setMessage("Succeed, " + msg);
		if (result != null)response.setResultdata(result);
		return response;
	}
	
	public static ResponseData errorResponse(String msg) {
		ResponseData response = new ResponseData();
		response.setCodestatus("E");
		response.setMessage("Error caused by : " + msg);
		return response;
	}

}
