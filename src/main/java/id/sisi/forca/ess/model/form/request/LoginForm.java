package id.sisi.forca.ess.model.form.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginForm implements Serializable{

	private static final long serialVersionUID = 7184164856057232073L;
	private String username;
	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
