package id.sisi.forca.ess.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "res_users")
public class User implements Serializable{

	private static final long serialVersionUID = -3655169709935510076L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "active")
	private boolean active;
	
	@Column(name = "login")
	private String login;
	
	@JsonIgnore
	@Column(name = "password_crypt")
	private String password_crypt;
	
	@Column(name = "isusefacedetection")
	private boolean isusefacedetection;
	
	@Column(name = "ischecklocation")
	private boolean ischecklocation;
	
	@Column(name = "device_id")
	private String device_id;
	
	@Column(name = "forca_person_id")
	private String forca_person_id;
	
	@Column(name = "longitude")
	private String longitude;
	
	@Column(name = "latitude")
	private String latitude;
	
	@Column(name = "distance")
	private String distance;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword_crypt() {
		return password_crypt;
	}

	public void setPassword_crypt(String password_crypt) {
		this.password_crypt = password_crypt;
	}

	public boolean isIsusefacedetection() {
		return isusefacedetection;
	}

	public void setIsusefacedetection(boolean isusefacedetection) {
		this.isusefacedetection = isusefacedetection;
	}

	public boolean isIschecklocation() {
		return ischecklocation;
	}

	public void setIschecklocation(boolean ischecklocation) {
		this.ischecklocation = ischecklocation;
	}

	public String getDevice_id() {
		return device_id;
	}

	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}

	public String getForca_person_id() {
		return forca_person_id;
	}

	public void setForca_person_id(String forca_person_id) {
		this.forca_person_id = forca_person_id;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

}
