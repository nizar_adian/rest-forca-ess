package id.sisi.forca.ess.health;

import com.codahale.metrics.health.HealthCheck;

public class ESSHealthCheck extends HealthCheck{

	@Override
	protected Result check() throws Exception {
		return Result.healthy();
	}

}
