package id.sisi.forca.ess;

import java.util.logging.Level;
import java.util.logging.Logger;

import id.sisi.forca.ess.config.ESSConfiguration;
import id.sisi.forca.ess.crypto.PasswordEncoder;
import id.sisi.forca.ess.filter.CustomAuthenticator;
import id.sisi.forca.ess.filter.CustomAuthenticatorFilter;
import id.sisi.forca.ess.health.ESSHealthCheck;
import id.sisi.forca.ess.model.entity.AccessToken;
import id.sisi.forca.ess.model.entity.User;
import id.sisi.forca.ess.resource.AuthResource;
import id.sisi.forca.ess.resource.SayingResource;
import id.sisi.forca.ess.resource.TransactionResource;
import id.sisi.forca.ess.service.ESSService;
import io.dropwizard.Application;
import io.dropwizard.auth.AuthDynamicFeature;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.hibernate.UnitOfWorkAwareProxyFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class ESSAplication extends Application<ESSConfiguration> {

	private final HibernateBundle<ESSConfiguration> hibernate = new HibernateBundle<ESSConfiguration>(User.class,
			AccessToken.class) {

		@Override
		public DataSourceFactory getDataSourceFactory(ESSConfiguration configuration) {
			return configuration.getDataSourceFactory();
		}

	};

	public static void main(String[] args) throws Exception {
		new ESSAplication().run(new String[] { "server", "config.yml" });
	}

	@Override
	public String getName() {
		return super.getName();
	}

	@Override
	public void initialize(Bootstrap<ESSConfiguration> bootstrap) {
		Logger.getLogger("org.hibernate").setLevel(Level.OFF);
		bootstrap.addBundle(hibernate);

	}

	@Override
	public void run(ESSConfiguration configuration, Environment environment) throws Exception {

		// HealtCheck
		final ESSHealthCheck essHealthCheck = new ESSHealthCheck();

		// Crypt
		final PasswordEncoder passwordEncoder = new PasswordEncoder();

		// Service
		final ESSService service = new ESSService(hibernate.getSessionFactory());
		// Register HealtCheck
		environment.healthChecks().register("template", essHealthCheck);
		
		// Register Filter Context Request
		CustomAuthenticator authenticator = new UnitOfWorkAwareProxyFactory(hibernate).create(CustomAuthenticator.class,
				new Class[] { ESSService.class }, new Object[] { service });
		CustomAuthenticatorFilter filter = new CustomAuthenticatorFilter(authenticator);
		environment.jersey().register(new AuthDynamicFeature(filter));

		// Register Resource
		environment.jersey().register(new SayingResource(service));
		environment.jersey().register(new AuthResource(service));
		environment.jersey().register(new TransactionResource(service));

	}

}
