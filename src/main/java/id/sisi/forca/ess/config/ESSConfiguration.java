package id.sisi.forca.ess.config;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

public class ESSConfiguration extends Configuration {

	@Valid
	@NotNull
	private DataSourceFactory database = new DataSourceFactory();

	@JsonProperty()
	public DataSourceFactory getDataSourceFactory() {
		return database;
	}

	@JsonProperty()
	public void setDatabase(DataSourceFactory database) {
		this.database = database;
	}

}
