package id.sisi.forca.ess.filter;

import java.io.IOException;
import java.util.Optional;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Response;

import org.eclipse.jetty.util.StringUtil;

import io.dropwizard.auth.AuthFilter;
import io.dropwizard.auth.AuthenticationException;

@PreMatching
@Priority(Priorities.AUTHENTICATION)
public class CustomAuthenticatorFilter extends AuthFilter {

	private static final String HEADER_TOKEN = "Forca-Token";
	private CustomAuthenticator customAuthenticator;

	public CustomAuthenticatorFilter(CustomAuthenticator customAuthenticator) {
		this.customAuthenticator = customAuthenticator;
	}

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		Optional<Object> authenticatedUser;
		try {
			Credentials credentials = getCredentials(requestContext);
			if (StringUtil.isEmpty(credentials.getToken())) {
				throw new WebApplicationException("Missing token credentials", Response.Status.UNAUTHORIZED);
			}
			authenticatedUser = customAuthenticator.authenticate(credentials);
		} catch (AuthenticationException e) {
			throw new WebApplicationException("Unable to validate credentials", Response.Status.UNAUTHORIZED);
		}

		if (!authenticatedUser.isPresent()) {
			throw new WebApplicationException("Credentials not valid", Response.Status.UNAUTHORIZED);
		}
	}

	private Credentials getCredentials(ContainerRequestContext requestContext) {
		Credentials credentials = new Credentials();

		try {
			String rawToken = requestContext.getHeaderString(HEADER_TOKEN);
			credentials.setToken(rawToken);
		} catch (Exception e) {
			throw new WebApplicationException("Unable to parse credentials", Response.Status.UNAUTHORIZED);
		}
		return credentials;
	}

}
