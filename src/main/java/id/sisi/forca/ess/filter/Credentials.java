package id.sisi.forca.ess.filter;

public class Credentials {
	
	private String token;
	
	public void setToken(String token) {
		this.token = token;
	}
	
	public String getToken() {
		return token;
	}

}
