package id.sisi.forca.ess.filter;

import java.util.Optional;

import org.eclipse.jetty.util.StringUtil;

import id.sisi.forca.ess.model.entity.AccessToken;
import id.sisi.forca.ess.service.ESSService;
import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.hibernate.UnitOfWork;

public class CustomAuthenticator implements Authenticator {

	private ESSService service;

	public CustomAuthenticator(ESSService service) {
		this.service = service;
	}

	@UnitOfWork
	@Override
	public Optional authenticate(Object credentials) throws AuthenticationException {
		Credentials credent = (Credentials) credentials;
		Optional<AccessToken> accessToken = service.getToken(StringUtil.nonNull(credent.getToken()));
		return accessToken;
	}

}
