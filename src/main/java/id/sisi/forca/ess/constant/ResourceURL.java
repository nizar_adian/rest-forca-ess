package id.sisi.forca.ess.constant;

public class ResourceURL {
	
	public static final String EXAMPLE = "/api/sample";
	public static final String USER = "/api/user";
	
	public static final String AUTH = "/api/ws/authentication/v1";
	public static final String TRANSACTION = "/api/ws/transaction/v1";

}
