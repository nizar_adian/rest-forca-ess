package id.sisi.forca.ess.resource;

import java.util.Optional;

import javax.annotation.security.PermitAll;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.codahale.metrics.annotation.Timed;

import id.sisi.forca.ess.constant.HeaderRequest;
import id.sisi.forca.ess.constant.ResourceURL;
import id.sisi.forca.ess.model.Saying;
import id.sisi.forca.ess.model.entity.User;
import id.sisi.forca.ess.model.form.request.LoginForm;
import id.sisi.forca.ess.model.form.response.ResponseData;
import id.sisi.forca.ess.service.ESSService;
import io.dropwizard.auth.Auth;
import io.dropwizard.hibernate.UnitOfWork;

@Path(ResourceURL.EXAMPLE)
@Produces(MediaType.APPLICATION_JSON)
public class SayingResource {
	
	private ESSService service;
	
	public SayingResource(ESSService service) {
		this.service = service;
	}
	
	public SayingResource() {
	}

	@GET
	@Timed
	@Path("/say")
	public Response sayHello(@QueryParam("name") Optional<String> name) {
		return Response.ok(ResponseData.successResponse(name.get(), null)).build();
	}

	@GET
	@UnitOfWork
	@Path("/ping")
	public Response ping(@Auth User user) {
		if (user != null) {
			return Response.ok(user).build();
		} else {
			return Response.ok().build();
		}
	}
	
	@PermitAll
	@POST
	@Path("/test-token")
	public Response testToken(@HeaderParam("Forca-Token") String token) {
		
		return Response.ok(token).build();
	}
	
	@POST
	@Path("/test")
	public Response test(@HeaderParam("Forca-Token") String token) {
		
		return Response.ok(token).build();
	}

}
