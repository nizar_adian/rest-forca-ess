package id.sisi.forca.ess.resource;

import javax.annotation.security.PermitAll;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import id.sisi.forca.ess.constant.HeaderRequest;
import id.sisi.forca.ess.constant.ResourceURL;
import id.sisi.forca.ess.model.form.request.LoginForm;
import id.sisi.forca.ess.model.form.response.ResponseData;
import id.sisi.forca.ess.service.ESSService;
import io.dropwizard.hibernate.UnitOfWork;

@Path(ResourceURL.AUTH)
@Produces(MediaType.APPLICATION_JSON)
public class AuthResource {

	private final ESSService service;

	public AuthResource(ESSService service) {
		this.service = service;
	}

	@UnitOfWork
	@POST
	@Path("/login")
	public Response login(LoginForm login) {
		ResponseData response = service.login(login);
		return Response.ok(response).build();
	}
	
	@PermitAll
	@POST
	@Path("/logout")
	@UnitOfWork
	public Response logout(@HeaderParam(HeaderRequest.TOKEN) String token) {
		service.logout(token);
		return Response.ok(ResponseData.successResponse("", null)).build();
	}

}
