package id.sisi.forca.ess.resource;

import javax.annotation.security.PermitAll;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import id.sisi.forca.ess.constant.HeaderRequest;
import id.sisi.forca.ess.constant.ResourceURL;
import id.sisi.forca.ess.model.form.request.AttendanceForm;
import id.sisi.forca.ess.model.form.request.AttendanceHistoryForm;
import id.sisi.forca.ess.model.form.request.DeviceForm;
import id.sisi.forca.ess.model.form.request.LocationForm;
import id.sisi.forca.ess.model.form.request.PersonForm;
import id.sisi.forca.ess.model.form.request.UserInfoForm;
import id.sisi.forca.ess.model.form.response.ResponseData;
import id.sisi.forca.ess.service.ESSService;
import io.dropwizard.hibernate.UnitOfWork;

@PermitAll
@Path(ResourceURL.TRANSACTION)
@Produces(MediaType.APPLICATION_JSON)
public class TransactionResource {

	private final ESSService service;

	public TransactionResource(ESSService service) {
		this.service = service;
	}
	
	@UnitOfWork
	@POST
	@Path("/getUserInfo")
	public Response getUserInfo(UserInfoForm form) {
		ResponseData responseData = service.getUserInfo(form);
		return Response.ok(responseData).build();
	}
	
	/*
	 * JSON Response
	 * {
		    "codestatus": "S",
		    "message": "1, Data Found",
		    "resultdata": [
		        {
		            "latitude": "-6.294170",
		            "longitude": "106.785245",
		            "name": "Work From Home",
		            "isusefacedetection": "N",
		            "forca_branch_id": "5",
		            "forca_distance": "10000000000000000000000000000000000000000000000"
		        }
		    ]
		}
	 */
	@POST
	@Path("/getLocation")
	public Response getLocation(@HeaderParam(HeaderRequest.TOKEN) String token, LocationForm form) {
		
		return Response.ok(form).build();
	}

	@POST
	@Path("/insertAttendance")
	public Response insertAttendance(@HeaderParam(HeaderRequest.TOKEN) String token, AttendanceForm form) {
		return Response.ok(form).build();
	}

	@POST
	@Path("/insertPersonID")
	public Response insertPersonID(@HeaderParam(HeaderRequest.TOKEN) String token, PersonForm form) {
		return Response.ok(form).build();
	}

	@POST
	@Path("/insertDeviceID")
	public Response insertDeviceID(@HeaderParam(HeaderRequest.TOKEN) String token, DeviceForm form) {
		return Response.ok(form).build();
	}

	@POST
	@Path("/getAttendanceHistory")
	public Response getAttendanceHistory(@HeaderParam(HeaderRequest.TOKEN) String token, AttendanceHistoryForm form) {
		return Response.ok(form).build();
	}

}
